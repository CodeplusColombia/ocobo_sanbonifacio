<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catedra extends Model
{
    protected $table = "e_catedra";
    protected $primaryKey = "Catedra";
    public $timestamps = false;

    protected $fillable = ["Catedra",
        "Grupo",
        "Credito",
        'Escala',
        'Profesor',
        'Lenguaje',
        'Intensidad',
        'Estado'];

    public function profesor()
    {
        return $this->belongsTo('App\Documento', 'Documento', 'Profesor');
    }

    public function grupo()
    {
        return $this->belongsTo('App\Grupo', 'Grupo', 'Grupo');
    }

    public function credito()
    {
        return $this->belongsTo('App\Credito', 'Credito', 'Credito');
    }

    public function alumnos()
    {
        return $this->hasMany('App\RegistroAlumno', 'Catedra', 'Catedra');
    }
}
