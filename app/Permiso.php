<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    protected $table = 'g_permiso';
    protected $primaryKey = 'Opcion';
    public $timestamps = false;

    protected $fillable = [
        'Usuario', 'Aplicacion', 'Opcion', 'Insertar', 'Actualizar', 'Borrar', 'Seleccionar', 'Ejecutar'
    ];

    public function perfiles()
    {
        return $this->hasMany('App\Perfil', 'Aplicacion', 'Aplicacion');
    }
}
