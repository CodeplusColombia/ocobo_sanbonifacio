<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistroAlumno extends Model
{
    protected $table = "e_registro";
    protected $primaryKey = "Registro";
    public $timestamps = false;

    protected $fillable = [
        "Registro",
        "Catedra",
        "Alumno",
        "Fecha",
        "Usuario",
        "Estado"
    ];

    public function Novedades()
    {
        return $this->hasMany('App\Novedad', 'Registro');
    }

    public function Documento()
    {
        return $this->belongsTo('App\Documento', 'Alumno', 'Interno');
    }

    public function catedra()
    {
        return $this->belongsTo('App\Catedra', 'Catedra', 'Catedra');
    }

}
