@extends('layouts.admin2')

@section('content')

<?php $message=Session::get('message')?>

@include('alerts.success')
@include('alerts.request')
@include('alerts.errors')

<style>
    .margin {
        margin-left: 50px;
        display: inline-block;
    }
</style>

    <br>
    <div class="busqueda">

        <div class="titulo_avanzado">

            <div class="page-header">
                <h1>Lista de Novedades</h1>
            </div>

        </div>

        <div class="buscar_avanzado">

        </div>

    </div>

    <table id="example" class="table table-striped table-bordered responsive nowrap" style="width:100%">
        <thead>
        <tr>
            <th>Novedad</th>
            <th>Fecha Inicio</th>
            <th>Fecha Final</th>
            <th>Nombre</th>
            <th>Anotacion</th>
            <th>Tipo</th>
            <th>Clasificación</th>
            <th>Privacidad</th>
            <th>Procedencia</th>
            <th>Acciones</th>
        </tr>
        </thead>

    </table>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').dataTable( {
                processing: true,
                serverSide: true,
                ajax: '{!! url('api/novedades') !!}',
                "fields":[
                    {
                        label:"Novedad",
                        name:"Novedad"
                    },
                    {
                        label:"Fecha Inicio",
                        name:"Fecha_inicio"
                    },
                    {
                        label:"Fecha Final",
                        name:"Fecha_final"
                    },
                    {
                        label:"Nombre",
                        name:"Nombre"
                    },
                    {
                        label:"Observacion",
                        name:"Observacion"
                    },
                    {
                        label:"Anotación",
                        name:"Desc_Anotacion"
                    },
                    {
                        label:"Clasificación",
                        name:"Desc_Clasificacion"
                    },

                    {
                        label:"Procedencia",
                        name:"Obs_Procedencia"
                    },
                ],
                "columns": [
                    { data: 'Novedad' },
                    { data: 'Fecha_inicio' },
                    { data: 'Fecha_final' },
                    { data: 'Nombre' },
                    { data: 'Observacion' },
                    { data: 'Desc_Anotacion' },
                    { data: 'Desc_Clasificacion' },
                    {
                        data: 'Privacidad',
                        render: function (data,type,row) {
                          if(type==='display')
                          {
                              if (data == 1)
                              {
                                  return '<input type="checkbox" class="Privacidad-active"  disabled checked>';
                              }
                              else
                              {
                                  return '<input type="checkbox" class="Privacidad-active"  disabled>';
                              }
                          }
                          return data;
                        },
                        className: "dt-body-center"
                    },
                    { data: 'Obs_Procedencia' },
                    {
                        data: 'Novedad',
                        name: 'Acciones',
                        render: function (data,type,row) {
                            return '<form action="/novedades/' + data + '" method="post">' +
                                        '{!! method_field("delete") !!}'+
                                        '{!! csrf_field() !!}'+
                                        '@if (Auth::user()->perfil->permiso->Actualizar === "1")<a href="/novedades/' + data + '/edit" class="btn btn-xs btn-success" xmlns="http://www.w3.org/1999/html">Editar</a> @endif' +
                                        '@if (Auth::user()->perfil->permiso->Actualizar === "1")<a href="/novedades/'+ data +'/tracing" class="btn btn-xs btn-warning" style="margin-left: 5px; margin-right: 5px;">Seguimiento</a>@endif' +
                                        '@if (Auth::user()->perfil->permiso->Borrar === "1")<input type="submit" class="btn btn-xs btn-danger" value="Eliminar">@endif' +
                                    '</form>';
                        },
                        orderable: false,
                        searchable: false
                    }
                ],
                dom: 'l<".margin" B>frtip',
                buttons: [
                    'excel', 'pdf', 'print'
                ],
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                },
                'rowCallback': function ( row , data ) {
                   //aca puedo hacer que ocurra algo despues de procesar
                }
            } );

        } );
    </script>

@stop
