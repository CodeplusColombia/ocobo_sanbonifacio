<?php

namespace App\Http\Controllers\API;

use App\Novedad;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class NovedadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Datatables::of(Novedad::query())->addColumn('Obs_Procedencia', function (Novedad $novedad) {
            if (isset($novedad->procedencia))
            {
                return $novedad->procedencia->Observacion;
            }
            else{
                return "No tiene procedencia";
            }
        })->addColumn('Desc_Clasificacion', function (Novedad $novedad) {
            if (isset($novedad->clasificacion))
            {
                return $novedad->clasificacion->Descripcion;
            }
            else{
                return "No tiene clasificacion";
            }
        })->addColumn('Desc_Anotacion', function (Novedad $novedad) {
            if (isset($novedad->anotacion))
            {
                return $novedad->anotacion->Descripcion;
            }
            else{
                return "No tiene anotacion";
            }
        })->addColumn('Nombre', function (Novedad $novedad) {
            if (isset($novedad->registro->documento->auxiliar))
            {
                if (strcasecmp($novedad->clasificacion->Valor_i,"R") == 0)
                {
                    return $novedad->registro->documento->auxiliar->Primer_nombre." ".
                        $novedad->registro->documento->auxiliar->Segundo_nombre." ".
                        $novedad->registro->documento->auxiliar->Primer_apellido." ".
                        $novedad->registro->documento->auxiliar->Segundo_apellido;
                }
                else if(strcasecmp($novedad->clasificacion->Valor_i,"C") == 0)
                {
                    return $novedad->registro->documento->auxiliar->Primer_nombre." ".
                        $novedad->registro->documento->auxiliar->Segundo_nombre." ".
                        $novedad->registro->documento->auxiliar->Primer_apellido." ".
                        $novedad->registro->documento->auxiliar->Segundo_apellido." - ".
                        $novedad->registro->catedra->grupo->curso->Descripcion." - ".
                        $novedad->registro->catedra->credito->asignatura->Descripcion;
                }

            }
            else{
                return "No tiene nombre";
            }
        })->addColumn('acciones', function (Novedad $novedad) {
                return '<a href="#edit-'.$novedad->Novedad.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Novedad  $novedad
     * @return \Illuminate\Http\Response
     */
    public function show(Novedad $novedad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Novedad  $novedad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Novedad $novedad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Novedad  $novedad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Novedad $novedad)
    {
        //
    }
}
