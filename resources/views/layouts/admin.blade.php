<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Administrador</title>

    <link rel="shortcut icon" href={!! asset('imagen/favicon.ico') !!}>

    {!!Html::script('https://code.jquery.com/jquery-3.3.1.min.js')!!}

    {!!Html::script('https://code.jquery.com/ui/1.12.1/jquery-ui.min.js')!!}

    {!!Html::style('css/bootstrap.min.css')!!}
    {!!Html::style('css/metisMenu.min.css')!!}
    {!!Html::style('css/sb-admin-2.css')!!}
    {!!Html::style('css/font-awesome.min.css')!!}
    {!!Html::style('css/ocobo.css')!!}
    {!!Html::style('https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css')!!}
    {!!Html::style('https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css')!!}

    {!!Html::style('https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css')!!}
    {!!Html::style('https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css')!!}

</head>

<body>

    <div id="wrapper">

        
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">OcoboSoft</a>
            </div>
           

            <ul class="nav navbar-top-links navbar-right" style="text-align: right">
                 <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i>Perfil Usuario</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i>Configuraciones</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="/logout"><i class="fa fa-sign-out fa-fw"></i> Cerrar Session</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> Novedad<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{!!URL::to('/novedades')!!}"><i class='fa fa-list-ol fa-fw'></i> Lista</a>
                                </li>
                                @if (Auth::user()->perfil->permiso->Insertar === "1")
                                <li>
                                    <a href="{!!URL::to('/novedades/create')!!}"><i class='fa fa-plus fa-fw'></i> Adicionar</a>
                                </li>
                                @endif
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

     </nav>

        <div id="page-wrapper">
            @yield('content')
        </div>

    </div>
    



    {!!Html::script('js/bootstrap.min.js')!!}
    {!!Html::script('js/metisMenu.min.js')!!}
    {!!Html::script('js/sb-admin-2.js')!!}

    {!!Html::script('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js')!!}
    {!!Html::script('https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js')!!}



    {!!Html::script('https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js')!!}
    {!!Html::script('https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js')!!}
    {!!Html::script('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js')!!}
    {!!Html::script('https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js')!!}
    {!!Html::script('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js')!!}
    {!!Html::script('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js')!!}
    {!!Html::script('https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js')!!}

</body>

</html>
