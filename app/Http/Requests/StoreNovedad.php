<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreNovedad extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Fecha_inicio' => 'required|date',
            'Fecha_final' => 'required|date',
            'Observacion' => 'required|string',
            'Clasificacion' => 'json|required',
            'Anotacion' => 'required|integer',
            'Privacidad' => 'required|integer',
            'Usuario' => 'required|string',
            'Estado' => 'required|integer',
            'Predecesor' => 'required|integer',
            'Registro' => 'required|integer',
        ];
    }
}
