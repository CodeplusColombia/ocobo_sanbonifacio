@if(Session::has('message-errors'))

    <div class="alert alert-danger alert-dismissible" role="alert" id="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">

        </button>
        {{Session::get('message-errors')}}
    </div>

@endif



<script type="text/javascript">
    $("#alert").show();
    window.setTimeout(function () {
        $("#alert").slideUp(500, function () {
            $("#alert").hide();
        });
    }, 5000);

</script>