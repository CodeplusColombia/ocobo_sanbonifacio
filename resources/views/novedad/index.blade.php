@extends('layouts.admin2')

<?php $message=Session::get('message')?>



@section('content')
    @include('alerts.success')
    @include('alerts.errors')

    <br>
    <div class="page-header">
        <h3>Novedad</h3>
    </div>
    @include('novedad.form.form')
@stop