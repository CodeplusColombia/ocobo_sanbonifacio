<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    protected $table = 'g_perfil';
    protected $primaryKey = 'Perfil';
    public $timestamps = false;

    protected $fillable = [
        'Usuario', 'Aplicacion', 'Perfil', 'Opciones', 'Indicador'
    ];

    public function usuario()
    {
        return $this->hasOne('App\User', 'Usuario', 'Usuario');
    }

    public function perfil()
    {
        return $this->hasOne('App\Registro', 'Registro', 'Perfil');
    }

    public function permiso()
    {
        return $this->belongsTo('App\Permiso', 'Aplicacion', 'Aplicacion');
    }
}
