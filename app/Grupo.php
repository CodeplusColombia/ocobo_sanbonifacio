<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    protected $table = "e_grupo";
    protected $primaryKey = "Grupo";
    public $timestamps = false;

    protected $fillable = ["Grupo",
        "Curso",
        "Periodo",
        'Jornada',
        'Cupos',
        'Director',
        'Coordinador',
        'Estado'];

    public function catedras()
    {
        return $this->hasMany('App\Catedra', 'Grupo', 'Grupo');
    }

    public function curso()
    {
        return $this->belongsTo('App\Curso', 'Curso', 'Curso');
    }
}
