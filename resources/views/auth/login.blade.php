<!doctype html>
<html lang="en">
<head>
    <title>Login - OcoboSoft</title>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="shortcut icon" href={!! asset('imagen/favicon.ico') !!}>

    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/login.css')}}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

    <script>
        window.onload=function(){
            var db = $("#db").val();
            $("#auxiliar").val(db);
        }
    </script>
</head>

<body>

<div class="login-page">

    {!! Form::open(['route' => 'login', 'method' => 'get', 'class' => 'form']) !!}

    <div class="logo">
        <img src="{{asset('imagen/logo.png')}}" width="250px" height="200px">
    </div>

    <select name="db" id="db">
        <option value="900190765">Sanbonifacio</option>
    </select>

    {!! Form::text('auxiliar', 900190765, ['id'=> 'auxiliar', 'placeholder' => 'Empresa', 'readonly' => 'false'])!!}
    {!! Form::text('usuario', null, ['id'=> 'usuario','placeholder' => 'Usuario']) !!}
    {!! Form::password('password', ['id'=> 'clave', 'placeholder' => 'Clave']) !!}

    {!! Form::submit('Ocobo' , ['id' => 'sub' , 'class' => 'sub'])!!}

    @if(Session::get('msg'))
        <p>{{ Session::get('msg') }}</p>
    @endif

    {!! Form::close() !!}

</div>


</body>

</html>
<script type="text/javascript">
    $("select[name='db']").on('change', function(){
        var db = $("#db").val();
        $("#auxiliar").val(db);
    });
</script>