<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Novedad extends Model
{
    //
    protected $table = "e_novedad";
    protected $primaryKey = "Novedad";
    public $timestamps = false;

    protected $fillable = ["Novedad",
        "Registro",
        "Fecha_inicio",
        "Fecha_final",
        "Observacion",
        "Clasificacion",
        "Valor",
        "Anotacion",
        "Privacidad",
        "Usuario",
        "Estado",
        "Procedencia",
        'Adjunto',
        'Adjunto_i',
        'Adjunto_ii',
        'Adjunto_iii',
        "Fecha_inicio",
        "Fecha_final"];

    protected $dates = ['Fecha_inicio','Fecha_final'];

    public function Procedencia()
    {
        return $this->belongsTo('App\Novedad', 'Procedencia');
    }

    public function Registro()
    {
        return $this->belongsTo('App\RegistroAlumno', 'Registro');
    }

    public function Clasificacion()
    {
        return $this->belongsTo('App\Registro', 'Clasificacion');
    }

    public function Anotacion()
    {
        return $this->belongsTo('App\Concepto', 'Anotacion');
    }

    public function Estado()
    {
        return $this->belongsTo('App\Registro', 'Estado');
    }
}
