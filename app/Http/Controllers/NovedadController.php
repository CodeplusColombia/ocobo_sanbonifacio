<?php

namespace App\Http\Controllers;

use App\Auxiliar;
use App\Novedad;
use App\Registro;
use App\Concepto;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreNovedad;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class NovedadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('novedad.lista');
        //return Auth::user()->perfil->perfil;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clasificacion = Registro::where([
            ['tabla', '=', 301],
            ['valor_iii', 'LIKE', 'EDUCACION'],
        ])->get(['registro', 'descripcion',DB::raw('CONCAT(valor_i, "-", valor_ii) AS valor'),'valor_i']);

        $sedes = Registro::where('tabla', '=', 201)->get(['Registro', 'descripcion']);

        $seguimientos = Registro::where([
            ['tabla', '=', 111],
            ['valor_i', 'LIKE', '%C%'],
        ])->get(['registro', 'descripcion']);

        $estado = Registro::where([
            ['tabla', '=', 801],
            ['valor_i', 'LIKE', '%N%'],
        ])->get(['registro', 'descripcion']);

        $anotaciones = Concepto::where([
            ['Unidad', 'LIKE', '30205'],
        ])->get(['Concepto', 'Descripcion']);


        return view('novedad.index',
            ['clasificaciones' => $clasificacion,
             'sedes' => $sedes,
             'seguimientos' => $seguimientos,
             'estado' => $estado,
             'anotaciones' => $anotaciones]);
    }

    public function tracing(Novedad $predecesor)
    {
        $clasificacion = Registro::where([
            ['tabla', '=', 301],
            ['valor_iii', 'LIKE', 'EDUCACION'],
        ])->get(['registro', 'descripcion',DB::raw('CONCAT(valor_i, "-", valor_ii) AS valor'),'valor_i']);

        $sedes = Registro::where('tabla', '=', 201)->get(['Registro', 'descripcion']);

        $seguimientos = Registro::where([
            ['tabla', '=', 111],
            ['valor_i', 'LIKE', '%C%'],
        ])->get(['registro', 'descripcion']);

        $estado = Registro::where([
            ['tabla', '=', 801],
            ['valor_i', 'LIKE', '%N%'],
        ])->get(['registro', 'descripcion']);

        $anotaciones = Concepto::where([
            ['Unidad', 'LIKE', '30205'],
        ])->get(['Concepto', 'Descripcion']);


        return view('novedad.index',
            ['clasificaciones' => $clasificacion,
                'sedes' => $sedes,
                'seguimientos' => $seguimientos,
                'estado' => $estado,
                'anotaciones' => $anotaciones,
                'predecesor'=>$predecesor]);
    }

    public function getAnotaciones()
    {
        $registro = Input::get('registro');
        $anotaciones = Concepto::where([
            ['Unidad', 'LIKE', '30205'],
            ['grupo','=',$registro],
        ])->get(['Concepto', 'Descripcion']);
        return $anotaciones;
    }

    public function autocomplete(){
        $term = Input::get('term');
        $valor_i = Input::get('valor_i');

        $results = array();

        if (strcasecmp($valor_i, "R") == 0)
        {
            $queries = Auxiliar::where('Identificacion',21002)
                ->where(function ($query) use ($term) {
                    $query->where('Primer_nombre', 'LIKE', '%'.$term.'%')
                        ->orWhere('Segundo_nombre', 'LIKE', '%'.$term.'%')
                        ->orWhere('Primer_apellido', 'LIKE', '%'.$term.'%')
                        ->orWhere('Segundo_apellido', 'LIKE', '%'.$term.'%');
                })
                ->get();

            foreach ($queries as $query)
            {
                $results[] = [ 'value' => $query->documento->registro[0]->Registro, 'label' => $query->Primer_nombre.' '.
                    $query->Segundo_nombre.' '.
                    $query->Primer_apellido.' '.
                    $query->Segundo_apellido ];
            }
        }
        else if ( strcasecmp($valor_i, "C") == 0)
        {
            $estudiantes = Auxiliar::where('Identificacion',21002)
                ->where(function ($query) use ($term) {
                    $query->where('Primer_nombre', 'LIKE', '%'.$term.'%')
                        ->orWhere('Segundo_nombre', 'LIKE', '%'.$term.'%')
                        ->orWhere('Primer_apellido', 'LIKE', '%'.$term.'%')
                        ->orWhere('Segundo_apellido', 'LIKE', '%'.$term.'%');
                })
                ->get();

            for ($i = 0; $i < count($estudiantes); $i++)
            {
                $estudiante = $estudiantes[$i];
                $registros = $estudiante->documento->registro;
                for ($j = 1; $j < count($registros); $j++)
                {
                    $registro = $registros[$j];
                    $results[] = [ 'value' => $registro->Registro, 'label' => $estudiante->Primer_nombre.' '.
                        $estudiante->Segundo_nombre.' '.
                        $estudiante->Primer_apellido.' '.
                        $estudiante->Segundo_apellido.' - '.
                        $registro->catedra->grupo->curso->Descripcion.' - '.
                        $registro->catedra->credito->asignatura->Descripcion];
                }
            }
        }

        return Response::json($results);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNovedad $request)
    {
        $datos = json_decode($request->Clasificacion,true);
        $Adjunto = null;
        $Adjunto_i = null;
        $Adjunto_ii = null;
        $Adjunto_iii = null;

        $id = Novedad::max('Novedad')+1;
        $nueva = new Novedad;
        $nueva->Novedad = $id;
        $nueva->Registro = $request->Registro;
        $nueva->Fecha_inicio = $request->Fecha_inicio;
        $nueva->Fecha_final = $request->Fecha_final;
        $nueva-> Observacion = $request->Observacion;
        $nueva-> Clasificacion = $datos['registro'];
        $nueva->Valor = 0;
        $nueva->Anotacion = $request->Anotacion;
        $nueva->Privacidad = $request->Privacidad;
        $nueva->Usuario = $request->Usuario;
        $nueva->Estado = $request->Estado;
        $nueva->Procedencia = $request->Predecesor;
        $nueva->adjunto = $Adjunto;
        $nueva->adjunto_i = $Adjunto_i;
        $nueva->adjunto_ii = $Adjunto_ii;
        $nueva->adjunto_iii = $Adjunto_iii;

        if ($request->hasFile('Adjunto')) {
            $archivo = \File::get($request->file('Adjunto'));
            $Adjunto = $request->file('Adjunto')->getClientOriginalName();
            $Ad = $nueva->Novedad.'/'.$request->file('Adjunto')->getClientOriginalName();
            \Storage::disk('ftp')->put($Ad, $archivo);

        }

        if ($request->hasFile('Adjunto_i')) {

            $archivo = \File::get($request->file('Adjunto_i'));
            $Adjunto_i = $request->file('Adjunto_i')->getClientOriginalName();
            $Ad_i = $nueva->Novedad.'/'.$request->file('Adjunto_i')->getClientOriginalName();
            \Storage::disk('ftp')->put($Ad_i, $archivo);

        }

        if ($request->hasFile('Adjunto_ii')) {

            $archivo = \File::get($request->file('Adjunto_ii'));
            $Adjunto_ii = $request->file('Adjunto_ii')->getClientOriginalName();
            $Ad_ii = $nueva->Novedad.'/'.$request->file('Adjunto_ii')->getClientOriginalName();

            \Storage::disk('ftp')->put($Ad_ii, $archivo);

        }

        if ($request->hasFile('Adjunto_iii')) {

            $archivo = \File::get($request->file('Adjunto_iii'));
            $Adjunto_iii = $request->file('Adjunto_iii')->getClientOriginalName();
            $Ad_iii = $nueva->Novedad.'/'.$request->file('Adjunto_iii')->getClientOriginalName();
            \Storage::disk('ftp')->put($Ad_iii, $archivo);

        }

        $nueva->adjunto = $Adjunto;
        $nueva->adjunto_i = $Adjunto_i;
        $nueva->adjunto_ii = $Adjunto_ii;
        $nueva->adjunto_iii = $Adjunto_iii;

        $nueva->save();

        Session::flash('message', 'Novedad creada correctamente');
        return Redirect::to('/novedades');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Novedad  $novedad
     * @return \Illuminate\Http\Response
     */
    public function show(Novedad $novedade)
    {
        return "cuenta la leyenda que aqui se muestra";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Novedad  $novedad
     * @return \Illuminate\Http\Response
     */
    public function edit(Novedad $novedade)
    {
        $clasificacion = Registro::where([
            ['tabla', '=', 301],
            ['valor_iii', 'LIKE', 'EDUCACION'],
        ])->get(['registro', 'descripcion',DB::raw('CONCAT(valor_i, "-", valor_ii) AS valor'),'valor_i']);

        $sedes = Registro::where('tabla', '=', 201)->get(['Registro', 'descripcion']);

        $seguimientos = Registro::where([
            ['tabla', '=', 111],
            ['valor_i', 'LIKE', '%C%'],
        ])->get(['registro', 'descripcion']);

        $estado = Registro::where([
            ['tabla', '=', 801],
            ['valor_i', 'LIKE', '%N%'],
        ])->get(['registro', 'descripcion']);

        $anotaciones = Concepto::where([
            ['Unidad', 'LIKE', '30205'],
        ])->get(['Concepto', 'Descripcion']);

       return view('novedad.edit',
           ['clasificaciones' => $clasificacion,
            'sedes' => $sedes,
            'seguimientos' => $seguimientos,
            'estado' => $estado,
            'anotaciones' => $anotaciones,
            'novedad' => $novedade,
            'predecesor'=>$novedade->procedencia]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Novedad  $novedad
     * @return \Illuminate\Http\Response
     */
    public function update(StoreNovedad $request, Novedad $novedade)
    {
        $datos = json_decode($request->Clasificacion,true);
        $Adjunto = null;
        $Adjunto_i = null;
        $Adjunto_ii = null;
        $Adjunto_iii = null;

        $novedade->Registro = $request->Registro;
        $novedade->Fecha_inicio = $request->Fecha_inicio;
        $novedade->Fecha_final = $request->Fecha_final;
        $novedade-> Observacion = $request->Observacion;
        $novedade-> Clasificacion = $datos['registro'];
        $novedade->Anotacion = $request->Anotacion;
        $novedade->Privacidad = $request->Privacidad;
        $novedade->Usuario = $request->Usuario;
        $novedade->Estado = $request->Estado;
        $novedade->Procedencia = $request->Predecesor;

        if(isset($novedade->Adjunto))
        {
            Storage::delete($novedade->Novedad.'/'.$novedade->Adjunto);
        }
        if(isset($novedade->Adjunto_i))
        {
            Storage::delete($novedade->Novedad.'/'.$novedade->Adjunto_i);
        }
        if(isset($novedade->Adjunto_ii))
        {
            Storage::delete($novedade->Novedad.'/'.$novedade->Adjunto_ii);
        }
        if(isset($novedade->Adjunto_iii))
        {
            Storage::delete($novedade->Novedad.'/'.$novedade->Adjunto_iii);
        }

        if ($request->hasFile('Adjunto')) {
            $archivo = \File::get($request->file('Adjunto'));
            $Adjunto = $request->file('Adjunto')->getClientOriginalName();
            $Ad = $novedade->Novedad.'/'.$request->file('Adjunto')->getClientOriginalName();
            \Storage::disk('ftp')->put($Ad, $archivo);

        }

        if ($request->hasFile('Adjunto_i')) {

            $archivo = \File::get($request->file('Adjunto_i'));
            $Adjunto_i = $request->file('Adjunto_i')->getClientOriginalName();
            $Ad_i = $novedade->Novedad.'/'.$request->file('Adjunto_i')->getClientOriginalName();
            \Storage::disk('ftp')->put($Ad_i, $archivo);

        }

        if ($request->hasFile('Adjunto_ii')) {

            $archivo = \File::get($request->file('Adjunto_ii'));
            $Adjunto_ii = $request->file('Adjunto_ii')->getClientOriginalName();
            $Ad_ii = $novedade->Novedad.'/'.$request->file('Adjunto_ii')->getClientOriginalName();

            \Storage::disk('ftp')->put($Ad_ii, $archivo);

        }

        if ($request->hasFile('Adjunto_iii')) {

            $archivo = \File::get($request->file('Adjunto_iii'));
            $Adjunto_iii = $request->file('Adjunto_iii')->getClientOriginalName();
            $Ad_iii = $novedade->Novedad.'/'.$request->file('Adjunto_iii')->getClientOriginalName();
            \Storage::disk('ftp')->put($Ad_iii, $archivo);

        }

        $novedade->save();

        Session::flash('message', 'Novedad modificada correctamente');

        return Redirect::to('/novedades');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Novedad  $novedade
     * @return \Illuminate\Http\Response
     */
    public function destroy(Novedad $novedade)
    {
        if(isset($novedade->Adjunto))
        {
            Storage::disk('ftp')->delete($novedade->Novedad.'/'.$novedade->Adjunto);
        }
        if(isset($novedade->Adjunto_i))
        {
            Storage::disk('ftp')->delete($novedade->Novedad.'/'.$novedade->Adjunto_i);
        }
        if(isset($novedade->Adjunto_ii))
        {
            Storage::disk('ftp')->delete($novedade->Novedad.'/'.$novedade->Adjunto_ii);
        }
        if(isset($novedade->Adjunto_iii))
        {
            Storage::disk('ftp')->delete($novedade->Novedad.'/'.$novedade->Adjunto_iii);
        }
        $novedade->delete();
        Session::flash('message', 'Se ha eliminado correctamente la novedad');
        return Redirect::to('/novedades');
    }
}
