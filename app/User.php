<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'g_usuario';
    protected $primaryKey = 'Usuario';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'auxiliar', 'usuario', 'password'
    ];


    public function getAuthPassword()
    {
        return $this->Password;
    }

    /**
     * Overrides the method to ignore the remember token.
     */
    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute)
        {
            parent::setAttribute($key, $value);
        }
    }

    public function perfil()
    {
        return $this->belongsTo('App\Perfil', 'Usuario', 'Usuario');
    }

    public function auxiliar()
    {
        return $this->belongsTo('App\Auxiliar', 'Auxiliar', 'Auxiliar');
    }
}
