<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asignatura extends Model
{
    protected $table = "e_asignatura";
    protected $primaryKey = "Asignatura";
    public $timestamps = false;

    protected $fillable = ["Asignatura",
        "Color",
        "Descripcion",
        'Observacion',
        'Nomenclatura',
        'Area',
        'Estado'];

    public function credito()
    {
        return $this->hasOne('App\Credito');
    }
}
