<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class e_novedad extends Model
{
    //
    protected $table = "e_novedad";
    protected $primaryKey = "Novedad";
    public $timestamps = false;

    protected $fillable = ["Novedad",
                           "Registro",
                           "Fecha_inicio",
                            "Fecha_final",
                            "Observacion",
                            "Clasificacion",
                            "Valor",
                            "Anotacion",
                            "Privacidad",
                            "Usuario",
                            "Estado",
                            "Procedencia",
                            'Adjunto',
                            'Adjunto_i',
                            'Adjunto_ii',
                            'Adjunto_iii'];


    protected $dates = ["Fecha_inicio", "Fecha_final"];
}
