@if(Session::has('message'))

    <div class="alert alert-success alert-dismissible" role="alert" id="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        </button>
        {{Session::get('message')}}
    </div>
@endif




<script type="text/javascript">
    $("#alert").show();
    window.setTimeout(function () {
        $("#alert").slideUp(500, function () {
            $("#alert").hide();
        });
    }, 5000);

</script>