@extends('layouts.admin')

<?php $message=Session::get('message')?>

@include('alerts.success')
@include('alerts.request')
@include('alerts.errors')

@section('content')
    <br>
    <div class="busqueda">

        <div class="titulo_avanzado">

            <div class="page-header">
                <h1>Lista de Creditos</h1>
            </div>

        </div>

        <div class="buscar_avanzado">

            <div class="form-row">
                <div class="form-group col-md-4">
                    {!! Form::select('buscar',['Nombre Tercero', 'Cedula Tercero'], null, ['class' => 'form-control', 'id' => 'buscar']) !!}
                </div>

                {{ Form::open(array('action' => "ControllerOperacion@lista", 'method' => 'GET') ) }}

                <div class="form-group col-md-8">

                    <div class="input-group">
                        {!! Form::text('cliente', null, ['class' => 'form-control', 'placeholder'=>'Nombre Tercero', 'id'=>'cliente']) !!}
                        {!! Form::hidden('cedula', null, ['class' => 'form-control', 'placeholder'=>'Cedula Tercero', 'id'=>'cedula']) !!}

                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit" id="buscar_boton" style="padding-top: 9px; padding-bottom: 9px">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                        </span>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

        </div>

    </div>

    <table class="table table-hover">
        <thead>
        <tr class="active">
            <th>#</th>
            <th>Comprobante</th>
            <th>Nombre</th>
            <th>Valor</th>
            <th>Operacion</th>
            <th>Adjunto</th>
            <th>Adjunto</th>
            <th>Adjunto</th>
            <th>Adjunto</th>
            <th colspan="3" style="text-align:center">Operaciones</th>
        </tr>

        </thead>
        @foreach($operaciones as $operacion)

            <tbody>
            <td nowrap>{{ $operacion->id }}</td>
            <td>{{ $operacion->t_comprobante }}</td>
            <td>{{ $operacion->nombre }}</td>
            <td nowrap>{{ number_format($operacion->valor)}}</td>
            <td nowrap>{{ $operacion->operacion}}</td>

            {{--Condicion para el Adjunto--}}
            @if(strstr( strtolower($operacion->adjunto), '.docx', true))
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto), $operacion->operacion ], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22508.png') !!}></button></td>
                {!! Form::close() !!}
            @endif

            @if(strstr( strtolower($operacion->adjunto), '.xls', true) OR strstr( strtolower($operacion->adjunto), '.xlsx', true))
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto), $operacion->operacion], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22507.png') !!}></button></td>
                {!! Form::close() !!}
            @endif

            @if(strstr( strtolower($operacion->adjunto), '.pdf', true))
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto), $operacion->operacion], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22506.png') !!}></button></td>
                {!! Form::close() !!}
            @endif

            @if(strstr( strtolower($operacion->adjunto), '.png', true) OR strstr( strtolower($operacion->adjunto), '.jpg', true)  OR strstr(strtolower($operacion->adjunto), '.jpeg', true))
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto), $operacion->operacion ], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22504.png') !!}></button></td>
                {!! Form::close() !!}
            @endif

            @if(strstr( strtolower($operacion->adjunto), '.txt', true))
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto), $operacion->operacion], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22503.png') !!}></button></td>
                {!! Form::close() !!}
            @endif

            @if($operacion->adjunto == '')
                <td nowrap><button type="submit" class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'" disabled="true"><img src={!! asset('imagen/22505.png') !!}></button></td>
            @endif

            {{--Condicion para el Adjunto_I--}}
            @if(strstr( strtolower($operacion->adjunto_i), '.docx', true) )
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto_i), $operacion->operacion], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22508.png') !!}></button></td>
                {!! Form::close() !!}
            @endif

            @if(strstr( strtolower($operacion->adjunto_i), '.xls', true) OR strstr( strtolower($operacion->adjunto_i), '.xlsx', true))
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto_i), $operacion->operacion], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22507.png') !!}></button></td>
                {!! Form::close() !!}
            @endif

            @if(strstr( strtolower($operacion->adjunto_i), '.pdf', true) )
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto_i), $operacion->operacion], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22506.png') !!}></button></td>
                {!! Form::close() !!}
            @endif

            @if(strstr( strtolower($operacion->adjunto_i), '.png', true) OR strstr(strtolower($operacion->adjunto_i), '.jpg', true)  OR strstr(strtolower($operacion->adjunto_i), '.jpeg', true))
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto_i), $operacion->operacion], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22504.png') !!}></button></td>
                {!! Form::close() !!}
            @endif

            @if(strstr( strtolower($operacion->adjunto_i), '.txt', true) )
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto_i), $operacion->operacion], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22503.png') !!}></button></td>
                {!! Form::close() !!}
            @endif

            @if($operacion->adjunto_i ==  '' )
                <td nowrap><button type="submit" class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'" disabled="true"><img src={!! asset('imagen/22505.png') !!}></button></td>
            @endif

            {{--Condicion para el Adjunto_II--}}
            @if(strstr( strtolower($operacion->adjunto_ii), '.docx', true))
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto_ii), $operacion->operacion], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22508.png') !!}></button></td>
                {!! Form::close() !!}
            @endif

            @if(strstr( strtolower($operacion->adjunto_ii), '.xls', true) OR strstr( strtolower($operacion->adjunto_ii), '.xlsx', true))
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto_ii )], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22507.png') !!}></button></td>
                {!! Form::close() !!}
            @endif

            @if(strstr( strtolower($operacion->adjunto_ii), '.pdf', true))
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto_ii), $operacion->operacion], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22506.png') !!}></button></td>
                {!! Form::close() !!}
            @endif

            @if(strstr( strtolower($operacion->adjunto_ii), '.png', true) OR strstr( strtolower($operacion->adjunto_ii), '.jpg', true)  OR strstr(strtolower($operacion->adjunto_ii), '.jpeg', true))
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto_ii), $operacion->operacion], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22504.png') !!}></button></td>
                {!! Form::close() !!}
            @endif

            @if(strstr( strtolower($operacion->adjunto_ii), '.txt', true))
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto_ii), $operacion->operacion], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22503.png') !!}></button></td>
                {!! Form::close() !!}
            @endif

            @if($operacion->adjunto_ii == '')
                <td nowrap><button type="submit" class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'" disabled="true"><img src={!! asset('imagen/22505.png') !!}></button></td>
            @endif

            {{--Condicion para el Adjunto_III--}}
            @if( strstr(strtolower($operacion->adjunto_iii), '.docx', true))
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto_iii), $operacion->operacion], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22508.png') !!}></button></td>
                {!! Form::close() !!}
            @endif

            @if( strstr( strtolower($operacion->adjunto_iii), '..xls', true) OR strstr( strtolower($operacion->adjunto_iii), '.xlsx', true))
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto_iii), $operacion->operacion], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22507.png') !!}></button></td>
                {!! Form::close() !!}
            @endif


            @if( strstr(strtolower($operacion->adjunto_iii), '.pdf', true))
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto_iii), $operacion->operacion], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22506.png') !!}></button></td>
                {!! Form::close() !!}
            @endif


            @if( strstr( strtolower($operacion->adjunto_iii), '.png', true) OR strstr( strtolower($operacion->adjunto_iii), '.jpg', true)  OR strstr( strtolower($operacion->adjunto_iii), '.jpeg', true))
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto_iii), $operacion->operacion ], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22504.png') !!}></button></td>
                {!! Form::close() !!}
            @endif


            @if( strstr( strtolower($operacion->adjunto_iii), '.txt', true))
                {!! Form::open(['route' => ['download', str_replace('moperacion/'.$operacion->operacion.'/', '' , $operacion->adjunto_iii), $operacion->operacion], 'method' => 'GET']) !!}
                <td nowrap><button class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'"><img src={!! asset('imagen/22503.png') !!}></button></td>
                {!! Form::close() !!}
            @endif

            @if($operacion->adjunto_iii == '')
                <td nowrap><button type="submit" class="btn btn-outline-secondary btn-sm" value="'.$response->Adjunto.'" disabled="true"><img src={!! asset('imagen/22505.png') !!}></button></td>
            @endif

                <td nowrap width="1%">
                    {!! html_entity_decode(link_to_route('operacion.edit', $title = '<i class="fa fa-edit"></i>' , $parameters = $operacion->operacion, $attributes = ['class' => 'btn btn-primary-u'])) !!}
                </td>

                <td nowrap width="2%">
                    {!! Form::open(['route' => ['operacion.destroy', $operacion->operacion], 'method' => 'DELETE']) !!}
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                </td>

            </tbody>
        @endforeach
    </table>
    <div align="right">
        {!! $operaciones->links() !!}
    </div>

    <script type="text/javascript">
        $('#buscar').on('change', function () {


            var cliente = document.getElementById("cliente");
            var cedula = document.getElementById("cedula");


            if( $('#buscar').val() == 0){
                cliente.setAttribute('type', 'text');
                cedula.setAttribute('type', 'hidden');
                $('#cliente').val('');
                $('#cedula').val('');
            }

            if( $('#buscar').val() == 1){
                cliente.setAttribute('type', 'hidden');
                cedula.setAttribute('type', 'text');
                $('#cliente').val('');
                $('#cedula').val('');

            }

            if( $('#buscar').val() == 2){
                cliente.setAttribute('type', 'hidden');
                cedula.setAttribute('type', 'hidden');
                $('#cliente').val('');
                $('#cedula').val('');
            }
        });


    </script>
@stop


