{!! Html::style('plugin/select2/dist/css/select2.css') !!}

<br>



{!! Form::open(['route' => ['novedades.update',$novedad->Novedad], 'method' => 'POST', 'files'=>true, 'enctype'=>'multipart/form-data']) !!}
{!! method_field("put")!!}
@if(isset($predecesor))
<input type="hidden" name="Predecesor" id='Predecesor' value="{{$predecesor->Novedad}}">
@else
    <input type="hidden" name="Predecesor" id='Predecesor' value="0">
@endif
<div class="formulario">

    @include('alerts.request')

    <div class="row">
        <div class="col-md-6 mb-3">
            <label for="validationServer01">Clasificación</label>
            <select name="Clasificacion" id="Clasificacion" class="form-control form-control-sm">
                @foreach($clasificaciones as $clasificacion)
                    @if ($clasificacion->registro == $novedad->clasificacion->Registro)
                        <option value='{"registro":{!! $clasificacion->registro !!},"valor_i":"{!! $clasificacion->valor_i !!}"}' selected>{!! $clasificacion->descripcion !!}</option>
                    @else
                        <option value='{"registro":{!! $clasificacion->registro !!},"valor_i":"{!! $clasificacion->valor_i !!}"}'>{!! $clasificacion->descripcion !!}</option>
                    @endif
                @endforeach
            </select>

        </div>
        <div class="col-md-6 mb-3">
            <label for="validationServer02">Anotación</label>
            <select name="Anotacion" id="Anotacion" class="form-control form-control-sm" placeholder="Selecciona">
                @foreach($anotaciones as $anotacion)
                    @if ($anotacion->Concepto === $novedad->anotacion->Concepto)
                        <option value="{!! $anotacion->Concepto !!}" selected>{!! $anotacion->Descripcion !!}</option>
                    @else
                        <option value="{!! $anotacion->Concepto !!}">{!! $anotacion->Descripcion !!}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 mb-3">
            <label for="validationServer03">Alumno</label>
            <input type="hidden" name="Registro" id='Registro' value="{{$novedad->registro->Registro}}">
            @if (strcasecmp($novedad->clasificacion->Valor_i,"R") == 0)
                {{ Form::text('Busqueda',
                    $novedad->registro->documento->auxiliar->Primer_nombre." ".
                    $novedad->registro->documento->auxiliar->Segundo_nombre." ".
                    $novedad->registro->documento->auxiliar->Primer_apellido." ".
                    $novedad->registro->documento->auxiliar->Segundo_apellido,
                    ['id' =>  'Busqueda', 'placeholder' => 'Ingrese el nombre', 'class' => 'form-control form-control-sm'])}}
            @elseif (strcasecmp($novedad->clasificacion->Valor_i,"C") == 0)
                {{ Form::text('Busqueda',
                    $novedad->registro->documento->auxiliar->Primer_nombre." ".
                    $novedad->registro->documento->auxiliar->Segundo_nombre." ".
                    $novedad->registro->documento->auxiliar->Primer_apellido." ".
                    $novedad->registro->documento->auxiliar->Segundo_apellido." - ".
                    $novedad->registro->catedra->credito->asignatura->Descripcion,
                    ['id' =>  'Busqueda', 'placeholder' => 'Ingrese el nombre', 'class' => 'form-control form-control-sm'])}}
            @endif

        </div>
        <div class="col-md-3 mb-3">
            <label for="validationServer04">Fecha Inicial</label>
            {!! Form::date('Fecha_inicio', $novedad->Fecha_inicio->format('Y-m-d'),  ['id'=>'Fecha', 'class' => 'form-control form-control-sm', 'required'])  !!}
        </div>
        <div class="col-md-3 mb-3">
            <label for="validationServer04">Fecha Final</label>
            {!! Form::date('Fecha_final', $novedad->Fecha_final->format('Y-m-d'),  ['id'=>'Fecha', 'class' => 'form-control form-control-sm', 'required'])  !!}
        </div>
    </div>

    <div class="row">

        <div class="col-md-4 mb-5">
            {!! Form::label('l_observacion', 'Observacion:   ') !!}
            {!! Form::textarea('Observacion', $novedad->Observacion,  ['id'=>'Observacion', 'class' => 'form-control form-control-sm textarea_table', 'size' => '10x3', 'required'])  !!}
        </div>

        <div class="col-lg-2 mb-3">
            <input type="hidden" name="Privacidad" id='Privacidad' value="0">
            @if ($novedad->Privacidad == 1)
                <input type="checkbox" class="field"  value="1" checked name="Privacidad" id="Privacidad">
            @else
                <input type="checkbox" class="field"  value="1" name="Privacidad" id="Privacidad">
            @endif
            {!! Form::label('lab_privacidad', 'Privacidad') !!}
        </div>
        <div class="col-md-3 mb-3">
            <label for="validationServer03">Usuario</label>
            {!! Form::text('Usuario', $novedad->Usuario, ['id'=>'Usuario', 'class' => 'form-control form-control-sm' , 'readonly' => 'true'] )  !!}
        </div>

        <div class="col-md-3 mb-3">
            {!! Form::label('l_transaccion', 'Estado', ['id'=>'l_transaccion']) !!}
            {!! Form::select('Estado' , $estado->pluck('descripcion', 'registro'), $novedad->estado->descripcion, ['id'=>'Estado', 'class' => 'form-control form-control-sm']) !!}
        </div>
    </div>

    <div class="row">

    </div>

    <div class="row">

        <div class="col-md-3 mb-3">
            <label for="validationServer04">Adjunto</label>
            {{ Form::file('Adjunto', ['id'=>'Adjunto_i', 'class' => 'form-control form-control-sm field']) }}
        </div>

        <div class="col-md-3 mb-3">
            <label for="validationServer05">Adjunto I</label>
            {{ Form::file('Adjunto_i', ['id'=>'Adjunto_ii' , 'class' => 'form-control form-control-sm field']) }}
        </div>
        <div class="col-md-3 mb-3">
            <label for="validationServer05">Adjunto II</label>
            {{ Form::file('Adjunto_ii', ['id'=>'Adjunto_iii', 'class' => 'form-control form-control-sm field']) }}
        </div>
        <div class="col-md-3 mb-3">
            <label for="validationServer05">Adjunto II</label>
            {{ Form::file('Adjunto_iii', ['id'=>'Adjunto_iii', 'class' => 'form-control form-control-sm field']) }}
        </div>
    </div>
    <br>
    <div class="form-group" align="right">
        <div class="col-md-100 mb-3">
            <button class="btn btn-primary" id="btnInsertar" type="submit">Guardar Cambios</button>
        </div>
    </div>
</div>

{!! Form::close() !!}

{{ Form::open(['action' => ['NovedadController@autocomplete'], 'method' => 'GET']) }}

{{ Form::submit('Search', array('class' => 'button expand')) }}
{{ Form::close() }}

<script src="{{ asset('plugin/select2/dist/js/select2.js') }}"></script>

<script>
    $('#Tercero').select2({
        width: '100%',
        language: {
            noResults: function (params) {
                return "No se encontro ningun resultado";
            }
        }
    });
</script>

<script>
    $(function()
    {
        $( "#Busqueda" ).autocomplete({
            source: function( request, response ) {
                $.ajax( {
                    url: "/search/autocomplete",
                    dataType: "json",
                    data: {
                        term: request.term,
                        valor_i:$.parseJSON( $("#Clasificacion").val() ) ['valor_i']
                    },
                    success: function( data ) {
                        response(data);
                    }
                } );
            },
            minLength: 2,
            select: function( event, ui ) {
                event.preventDefault();
                $('#Registro').val(ui.item.value);
                $('#Busqueda').val(ui.item.label);
            }
        } );
    });
</script>

<script type="text/javascript">
    $('#Valor').keyup(function(event) {

        // skip for arrow keys
        if(event.which >= 37 && event.which <= 40){
            event.preventDefault();
        }

        $(this).val(function(index, value) {
            return value
                .replace(/\D/g, "")
                //.replace(/([0-9])([0-9]{2})$/, '$1.$2')
                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".")
                ;
        });
    });
</script>


<script type="text/javascript">
    $("select[name='Comprobante']").on('change', function(){
        var comprobante = $(this).val();
        if(comprobante) {
            $.ajax({
                url: '/tercero/'+comprobante,
                type: "GET",
                dataType: "json",
                success:function(data) {
                    $('select[name="Tercero"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="Tercero"]').attr('disabled', false)
                        $('#Tercero').append('<option value="'+ value.auxiliar +'">'+ value.nombre +'</option>').trigger('change.select2');
                    });

                }
            });
        }else{
            $('select[name="Tercero"]').empty();
        }
    });

</script>

<script type="text/javascript">
    $("select[name='Clasificacion']").on('change', function(){
        var comprobante = $.parseJSON( $(this).val() ) ['registro'];
        if(comprobante) {
            $.ajax({
                url: '/search/anotaciones',
                type: "GET",
                dataType: "json",
                data: { registro: comprobante },
                success:function(data) {
                    $('select[name="Anotacion"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="Anotacion"]').attr('disabled', false)
                        $('select[name="Anotacion"]').append('<option value="'+ value.Concepto +'">'+ value.Descripcion+'</option>');
                    });

                }
            });
            $('#Registro').val("");
            $('#Busqueda').val("");
        }else{
            $('select[name="Anotacion"]').empty();
        }
    });
</script>


<script type="text/javascript">
    $('#Fecha').on('change', function () {
        var fecha = $('#Fecha').val(); // Formato fecha = 2017-12-21
        $.ajax({
            url: '/Fecha/'+fecha,
            type: "GET",
            dataType: "json",
            success:function(data) {
                if(data != true){
                    $('#btnInsertar').attr("disabled", "disabled");
                }
            }
        });
    });

</script>

