<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\mo_operacion;

class ControllerAuxiliar extends Controller
{
    //

    public function sincronizarAuxiliar(){
        $auxiliar = DB::table('g_auxiliar')->select('Identificacion', 'Auxiliar', 'Primer_nombre', 'Segundo_nombre', 'Primer_apellido', 'Segundo_apellido', 'Direccion', 'Municipio', 'Telefono', 'Telefono_i', 'E_mail')->get();

        return json_encode($auxiliar);

    }

    public function actualizarAuxiliar($auxiliar){

        $resultado = DB::statement("UPDATE g_auxiliar SET Sincronizado = 1 WHERE Auxiliar = $auxiliar");
        if($resultado == true){
            return json_encode('Sincronizado');
        }
        return json_encode('No Sincronizado');

    }

    public function sincronizarOperacionMovil(Request $request){
        
        $Adjunto = null;
        $Adjunto_i = null;
        $Adjunto_ii = null;
        $Adjunto_iii = null;

        $opc = DB::insert("INSERT INTO `mo_operacion` (`Operacion`, `Empresa`, `Comprobante`, `Numero`, `Fecha`, `Registro`, `Seguimiento`, ".
            " `Auxiliar`, `Valor`, `Observacion`, `Proceso`, `Transaccion`, `Referencia`, `Referencia_i`, `Referencia_ii`, ".
            " `Cuenta`, `Sede`, `Usuario`, `Procedencia`, `Opcion`, `Opcion_i`, `Opcion_ii`, `Opcion_iii`, ".
            " `Adjunto`, `Adjunto_i`, `Adjunto_ii`, `Adjunto_iii`, `Mo_operacion`,`IP_movil`, `Sincronizado` ) ".
            " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            [   '0', $request->Empresa , $request->Comprobante, '0', $request->Fecha, $request->Registro, $request->Seguimiento,
                $request->Auxiliar,  $request->Valor, $request->Observacion, $request->Proceso, $request->Transaccion,
                'a','a', 'a' , $request->Auxiliar, $request->Sede, $request->Usuario,
                '1', $request->Opcion, $request->Opcion_i, $request->Opcion_ii, $request->Opcion_iii,
                $Adjunto, $Adjunto_i, $Adjunto_ii, $Adjunto_iii,
                $request->Mo_operacion, $request->IP_movil, $request->Sincronizado]);

        $op = mo_operacion::all();

        if ($request->hasFile('Adjunto')) {
            $archivo = \File::get($request->file('Adjunto'));
            $Adjunto = 'moperacion'.'/'.$op->last()->Operacion.'/'.$request->file('Adjunto')->getClientOriginalName();
            $Ad = $op->last()->Operacion.'/'.$request->file('Adjunto')->getClientOriginalName();
            \Storage::disk('ftp')->put($Ad, $archivo);
        }

        if ($request->hasFile('Adjunto_i')) {

            $archivo = \File::get($request->file('Adjunto_i'));
            $Adjunto_i = 'moperacion'.'/'.$op->last()->Operacion.'/'.$request->file('Adjunto_i')->getClientOriginalName();
            $Ad_i = $op->last()->Operacion.'/'.$request->file('Adjunto_i')->getClientOriginalName();
            \Storage::disk('ftp')->put($Ad_i, $archivo);

        }

        if ($request->hasFile('Adjunto_ii')) {

            $archivo = \File::get($request->file('Adjunto_ii'));
            $Adjunto_ii = 'moperacion'.'/'.$op->last()->Operacion.'/'.$request->file('Adjunto_ii')->getClientOriginalName();
            $Ad_ii = $op->last()->Operacion.'/'.$request->file('Adjunto_ii')->getClientOriginalName();

            \Storage::disk('ftp')->put($Ad_ii, $archivo);

        }

        if ($request->hasFile('Adjunto_iii')) {

            $archivo = \File::get($request->file('Adjunto_iii'));
            $Adjunto_iii = 'moperacion'.'/'.$op->last()->Operacion.'/'.$request->file('Adjunto_iii')->getClientOriginalName();
            $Ad_iii = $op->last()->Operacion.'/'.$request->file('Adjunto_iii')->getClientOriginalName();
            \Storage::disk('ftp')->put($Ad_iii, $archivo);

        }

        DB::table('mo_operacion')->where('Operacion', $op->last()->Operacion)->update([ 'Adjunto' => $Adjunto , 'Adjunto_i' => $Adjunto_i, 'Adjunto_ii' => $Adjunto_ii , 	'Adjunto_iii' => $Adjunto_iii] );


        return response()->json($opc);
    }

    public function nube($id, $id_android){
        $idnube = (int)$id;
        $nube_operacion = DB::select('SELECT Operacion FROM mo_operacion WHERE Mo_operacion = ? AND IP_Movil = ?', [$idnube, $id_android]);

        return $nube_operacion[0]->Operacion;
    }
}
