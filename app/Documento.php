<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    protected $table = "documento";
    protected $primaryKey = "Documento";
    public $timestamps = false;

    protected $fillable = [
        "Empresa",
        "Documento",
        "Numero",
        "Fecha",
        "Seguimiento",
        "Sede",
        "Usuario",
        "Operacion",
        "Auxiliar",
        "Inmueble",
        "Inicio",
        "Terminacion",
        'Vencimiento',
        'Valor',
        'Interno',
        'Fisico',
        "E_mail",
        "App",
        'Electronico',
        'Envio',
        'Cuenta',
        "Clasificacion",
        "Anterior"
    ];

    public function Registro()
    {
        return $this->hasMany('App\RegistroAlumno', 'Alumno', 'Interno');
    }

    public function catedras()
    {
        return $this->hasMany('App\Catedra', 'Profesor', 'Documento');
    }

    public function Auxiliar()
    {
        return $this->belongsTo('App\Auxiliar', 'Auxiliar', 'Auxiliar');
    }
}
