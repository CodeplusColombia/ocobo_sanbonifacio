<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grado extends Model
{
    protected $table = "e_grado";
    protected $primaryKey = "Grado";
    public $timestamps = false;

    protected $fillable = ["Grado",
        "Programa",
        "Descripcion",
        'Observacion',
        'Nomenclatura',
        'Prerrequisito',
        'Clasificacion',
        'Estado'];

    public function creditos()
    {
        return $this->hasMany('App\Credito', 'Grado', 'Grado');
    }
}
