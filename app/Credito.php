<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credito extends Model
{
    protected $table = "e_credito";
    protected $primaryKey = "Credito";
    public $timestamps = false;

    protected $fillable = ["Credito",
        "Grado",
        "Asignatura",
        'Observacion',
        'Obligatoria',
        'Prerrequisito',
        'Lenguaje',
        'Aula',
        'Intensidad',
        'Estado'];

    public function prerrequisito()
    {
        return $this->belongsTo('App\Credito', 'Prerrequisito');
    }

    public function asignatura()
    {
        return $this->belongsTo('App\Asignatura', 'Asignatura');
    }

    public function catedras()
    {
        return $this->hasMany('App\Catedra', 'Credito', 'Credito');
    }

    public function grado()
    {
        return $this->belongsTo('App\Grado', 'Grado');
    }
}
