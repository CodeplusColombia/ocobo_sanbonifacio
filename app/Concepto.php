<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concepto extends Model
{
    protected $table = "concepto";
    protected $primaryKey = "concepto";
    public $timestamps = false;

    protected $fillable = ["Concepto",
        "Descripcion",
        "Grupo",
        "Parametro",
        "Unidad"];

    public function Novedades()
    {
        return $this->hasMany('App\Novedad', 'Anotacion');
    }
}
