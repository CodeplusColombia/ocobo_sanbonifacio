<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auxiliar extends Model
{
    protected $table = "g_auxiliar";
    protected $primaryKey = "Auxiliar";
    public $timestamps = false;

    protected $fillable = [
        "Auxiliar",
        "Digito_chequeo",
        "Identificacion",
        "Nacimiento",
        "Expedicion",
        "Sitio",
        "Sexo",
        "Estado_civil",
        "Regimen",
        "Primer_nombre",
        "Segundo_nombre",
        "Primer_apellido",
        'Segundo_apellido',
        'Direccion',
        'Municipio',
        'Ubicacion',
        "Barrio",
        "Postal",
        'coordenada',
        'Telefono',
        'Telefono_i',
        "E_mail",
        "E_mail_i",
        "Www",
        "Grupo"
    ];

    public function Documento()
    {
        return $this->hasOne('App\Documento', 'Auxiliar', 'Auxiliar');
    }

    public function usuario()
    {
        return $this->hasMany('App\User', 'Auxiliar', 'Auxiliar');
    }

    public function identificacion()
    {
        return $this->belongsTo('App\Registro', 'Identificacion', 'Registro');
    }

    public function grupo()
    {
        return $this->belongsTo('App\Registro', 'Grupo', 'Registro');
    }
}
