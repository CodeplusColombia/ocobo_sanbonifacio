<?php

namespace App\Http\Controllers;

use App\Models\e_novedad;
use App\Models\mo_operacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Redirect;

class ControllerNovedad extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clasificacion = DB::table('g_registro')->select('registro', 'descripcion',DB::raw('CONCAT(valor_i, "-", valor_ii)'))
            ->where([['tabla', 301], [DB::raw('UPPER(valor_iii)'), 'LIKE', 'EDUCACION']])
            ->orderBy('registro')->get();

        $sedes = DB::table('g_registro')->select('Registro', 'descripcion')->where('tabla', '=','201')->orderBy('Registro')->get();

        $seguimientos = DB::table('g_registro')->select('Registro', 'descripcion')->where([['tabla', '=','111'], ['valor_i', 'LIKE','%C%']])->orderBy('Registro')->get();

        $estado = DB::table('g_registro')->select('registro','descripcion')->where([['tabla', 801], ['valor_i', 'LIKE', '%N%']])->orderBy('registro')->get();

        return view('novedad.index')->with(array('clasificaciones' => $clasificacion, 'sedes' => $sedes, 'seguimientos' => $seguimientos, 'estado' => $estado));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Adjunto = null;
        $Adjunto_i = null;
        $Adjunto_ii = null;
        $Adjunto_iii = null;

        if($request->Privacidad == ''){
            $request->Privacidad = 0;
        }

        DB::insert("INSERT INTO `e_novedad`(`Novedad`, `Registro`, `Fecha_inicio`, `Fecha_final`, `Observacion`, `Clasificacion`, `Valor`, `Anotacion`, ".
            " `Privacidad`, `Usuario`, `Estado`, `Procedencia`, `adjunto`, `adjunto_i`, `adjunto_ii`, `adjunto_iii`) ".
            " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",

            [   0, 123, $request->Fecha_inicio, $request->Fecha_final, $request->Observacion, $request->Clasificacion,  0,
                $request->Anotacion, $request->Privacidad, $request->Usuario, $request->Estado, 1, $Adjunto, $Adjunto_i, $Adjunto_ii, $Adjunto_iii ]) ;


        $en = e_novedad::all();

        if ($request->hasFile('Adjunto')) {
            $archivo = \File::get($request->file('Adjunto'));
            $Adjunto = $request->file('Adjunto')->getClientOriginalName();
            $Ad = $en->last()->Novedad.'/'.$request->file('Adjunto')->getClientOriginalName();
            \Storage::disk('ftp')->put($Ad, $archivo);

        }

        if ($request->hasFile('Adjunto_i')) {

            $archivo = \File::get($request->file('Adjunto_i'));
            $Adjunto_i = $request->file('Adjunto_i')->getClientOriginalName();
            $Ad_i = $en->last()->Novedad.'/'.$request->file('Adjunto_i')->getClientOriginalName();
            \Storage::disk('ftp')->put($Ad_i, $archivo);

        }

        if ($request->hasFile('Adjunto_ii')) {

            $archivo = \File::get($request->file('Adjunto_ii'));
            $Adjunto_ii = $request->file('Adjunto_ii')->getClientOriginalName();
            $Ad_ii = $en->last()->Novedad.'/'.$request->file('Adjunto_ii')->getClientOriginalName();

            \Storage::disk('ftp')->put($Ad_ii, $archivo);

        }

        if ($request->hasFile('Adjunto_iii')) {

            $archivo = \File::get($request->file('Adjunto_iii'));
            $Adjunto_iii = $request->file('Adjunto_iii')->getClientOriginalName();
            $Ad_iii = $en->last()->Novedad.'/'.$request->file('Adjunto_iii')->getClientOriginalName();
            \Storage::disk('ftp')->put($Ad_iii, $archivo);

        }


        DB::table('e_novedad')->where('Novedad', $en->last()->Novedad)->update([ 'Adjunto' => $Adjunto , 'Adjunto_i' => $Adjunto_i, 'Adjunto_ii' => $Adjunto_ii , 'Adjunto_iii' => $Adjunto_iii] );

        Session::flash('message', 'Novedad creada correctamente');
        return Redirect::to('/novedad');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $eneracion = mo_operacion::find($id);

        $eneracion->Valor = number_format($eneracion->Valor, 0);
        $eneracion->Valor = str_replace(',', '.',$eneracion->Valor);

        $comprobante_id = substr($eneracion->Comprobante, 0, 2);

        $comprobante = DB::table('base as b')->select('b.base AS Registro', 'b.descripcion', 'b.Transaccion AS valor_i')
            ->join('g_registro as r', 'b.forma', 'r.Registro')
            ->where([
                ['b.tipo', '=', 11903],
                ['r.valor_i' ,'like', '%E%'],
                ['b.base', '=', $eneracion->Comprobante]])
            ->orderBy('b.base')->get();

        $comprobantes = DB::table('base as b')->select('b.base AS Registro', 'b.descripcion', 'b.Transaccion AS valor_i')
            ->join('g_registro as r', 'b.forma', 'r.Registro')
            ->where([
                ['b.tipo', '=', 11903],
                ['r.valor_i' ,'like', '%E%'],
                ['b.base', '<>', $eneracion->Comprobante]])
            ->orderBy('b.base')->get();


        $proceso = DB::select("SELECT clase AS Registro, descripcion FROM clase WHERE clase=0 " .
            " UNION SELECT DISTINCT o.clase AS Registro, c.descripcion" .
            " FROM operacion o, base b, clase c" .
            " WHERE o.base=b.base AND o.clase=c.clase AND o.base=$eneracion->Comprobante AND o.base!=o.clase" .
            " AND o.causacion IN(SELECT Registro FROM g_registro WHERE tabla=125 AND valor_i LIKE 'Comprobante%')" .
            " AND o.operacion BETWEEN {$comprobante_id}000 AND {$comprobante_id}999 AND b.forma=18501".
            " UNION SELECT Proceso AS Registro, comentario AS descripcion FROM proceso WHERE clase=$eneracion->Comprobante ORDER BY 1;");

        $proceso = collect($proceso);

        $tercero =  DB::select("SELECT DISTINCT v.auxiliar, v.nombre FROM base b, grupo_base g, grupo_auxiliar a, v_auxiliar v
                                WHERE b.Base = g.Base AND g.Grupo = a.Grupo AND a.Auxiliar = v.auxiliar AND v.auxiliar = ?", [$eneracion->Auxiliar]);

        $terceros =  DB::select("SELECT DISTINCT v.auxiliar, v.nombre FROM base b, grupo_base g, grupo_auxiliar a, v_auxiliar v
                                WHERE b.Base = g.Base AND g.Grupo = a.Grupo AND a.Auxiliar = v.auxiliar AND v.auxiliar <> ?", [$eneracion->Auxiliar]);

        $sede = DB::table('g_registro')->select('Registro', 'descripcion')->where([['tabla', '=','201'], ['Registro', '=', $eneracion->Sede] ])->orderBy('Registro')->get();

        $sedes = DB::table('g_registro')->select('Registro', 'descripcion')->where([['tabla', '=','201'], ['Registro', '<>', $eneracion->Sede] ])->orderBy('Registro')->get();


        $seguimiento = DB::table('g_registro')->select('Registro', 'descripcion')->where([['tabla', '=','111'], ['valor_i', 'LIKE','%C%'], ['Registro', '=', $eneracion->Seguimiento ]])->orderBy('Registro')->get();

        $seguimientos = DB::table('g_registro')->select('Registro', 'descripcion')->where([['tabla', '=','111'], ['valor_i', 'LIKE','%C%'], ['Registro', '<>', $eneracion->Seguimiento ]])->orderBy('Registro')->get();


        $transaccion = DB::select("SELECT p.Numero as Registro, CONCAT(c.d_cuenta,' (',m.descripcion,')') as descripcion, c.Cuenta as valor_i
                                   FROM v_cuenta c, transaccion p, g_registro t, g_registro m, g_registro r, g_registro ri, g_registro rii 
                                  WHERE c.Auxiliar= $eneracion->Empresa
                                  AND   p.Transaccion=$eneracion->Transaccion
                                  AND   c.Cuenta=p.Cuenta 
                                  AND   p.Transaccion  = t.Registro   AND t.tabla   = 182 
                                  AND   p.medio        = m.Registro   AND m.tabla   = 183 
                                  AND   p.Referencia   = r.Registro   AND r.tabla   = 184 
                                  AND   p.Referencia_i = ri.Registro  AND ri.tabla  = 184 
                                  AND   p.Referencia_ii= rii.Registro AND rii.tabla = 184 
                                  ORDER BY p.Transaccion,p.Numero");



        return view('operacion.edit', compact('operacion', 'comprobante', 'comprobantes', 'sede', 'sedes', 'seguimiento','seguimientos', 'proceso', 'tercero', 'terceros', 'transaccion', 'Fecha'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $eneracion = mo_operacion::find($id);
        $enAr = mo_operacion::find($id);

        $Adjunto = null;
        $Adjunto_i = null;
        $Adjunto_ii = null;
        $Adjunto_iii = null;


        //Si el archivo va hacer cambiado

        if ($request->hasFile('Adjunto')) {

            $archivo = \File::get($request->file('Adjunto'));
            $Adjunto = $request->file('Adjunto')->getClientOriginalName();
            $Ad = $enAr->Operacion.'/'.$request->file('Adjunto')->getClientOriginalName();
            \Storage::disk('ftp')->put($Ad, $archivo);

        }

        if ($request->hasFile('Adjunto_i')) {

            $archivo = \File::get($request->file('Adjunto_i'));
            $Adjunto_i = $request->file('Adjunto_i')->getClientOriginalName();
            $Ad_i = $enAr->Operacion.'/'.$request->file('Adjunto_i')->getClientOriginalName();
            \Storage::disk('ftp')->put($Ad_i, $archivo);

        }

        if ($request->hasFile('Adjunto_ii')) {

            $archivo = \File::get($request->file('Adjunto_ii'));
            $Adjunto_ii = $request->file('Adjunto_ii')->getClientOriginalName();
            $Ad_ii = $enAr->Operacion.'/'.$request->file('Adjunto_ii')->getClientOriginalName();
            \Storage::disk('ftp')->put($Ad_ii, $archivo);

        }

        if ($request->hasFile('Adjunto_iii')) {

            $archivo = \File::get($request->file('Adjunto_iii'));
            $Adjunto_iii = $request->file('Adjunto_iii')->getClientOriginalName();
            $Ad_iii = $enAr->Operacion.'/'.$request->file('Adjunto_iii')->getClientOriginalName();
            \Storage::disk('ftp')->put($Ad_iii, $archivo);

        }

        $eneracion->fill($request->all());
        $eneracion->Auxiliar = $request->Tercero;
        $eneracion->Valor = str_replace(',', '',$request->Valor);


        if($request->Opcion == NULL){
            $eneracion->Opcion = 0;
        }

        if($request->Opcion_i == NULL){
            $eneracion->Opcion_i = 0;
        }

        if($request->Opcion_ii == NULL){
            $eneracion->Opcion_ii = 0;
        }

        if($request->Opcion_iii == NULL){
            $eneracion->Opcion_iii = 0;
        }


        //Si el archivo es null dejan el mismo archivo.
        if($request->Adjunto == NULL){
            $eneracion->Adjunto = $enAr->Adjunto;
        }

        if($request->Adjunto_i == NULL){
            $eneracion->Adjunto_i = $enAr->Adjunto_i;
        }

        if($request->Adjunto_ii == NULL){
            $eneracion->Adjunto_ii = $enAr->Adjunto_ii;
        }

        if($request->Adjunto_iii == NULL){
            $eneracion->Adjunto_iii = $enAr->Adjunto_iii;
        }


        //Si van a realizar alguno cambio
        if($Adjunto != NULL){
            $eneracion->Adjunto = $Adjunto;
        }

        if($Adjunto_i != NULL){
            $eneracion->Adjunto_i = $Adjunto_i;
        }

        if($Adjunto_ii != NULL) {
            $eneracion->Adjunto_ii = $Adjunto_ii;
        }

        if($Adjunto_iii != NULL){
            $eneracion->Adjunto_iii = $Adjunto_iii;
        }

        $valor = str_replace('.', '',$request->Valor);

        $eneracion->Valor = $valor;


        $eneracion->save();


        Session::flash('message', 'Operacion actualizada correctamente');
        return Redirect::to('/lista');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        mo_operacion::destroy($id);
        Session::flash('message', 'Operacion eliminada correctamente');
        return Redirect::back();
    }

    public function transaccion($id){

        $empresa = session()->get('empresa');
        $transaccion = DB::select("SELECT p.Numero as Registro, CONCAT(c.d_cuenta,' (',m.descripcion,')') as descripcion, c.Cuenta as valor_i
                                   FROM v_cuenta c, transaccion p, g_registro t, g_registro m, g_registro r, g_registro ri, g_registro rii 
                                  WHERE c.Auxiliar= $empresa
                                  AND   p.Transaccion=$id
                                  AND   c.Cuenta=p.Cuenta 
                                  AND   p.Transaccion  = t.Registro   AND t.tabla   = 182 
                                  AND   p.medio        = m.Registro   AND m.tabla   = 183 
                                  AND   p.Referencia   = r.Registro   AND r.tabla   = 184 
                                  AND   p.Referencia_i = ri.Registro  AND ri.tabla  = 184 
                                  AND   p.Referencia_ii= rii.Registro AND rii.tabla = 184 
                                  ORDER BY p.Transaccion,p.Numero");
        return $transaccion;
    }

    public function proceso($id)
    {

        $procesos = DB::select("SELECT concepto AS registro, descripcion FROM concepto WHERE unidad='30205' AND grupo = $id ORDER BY 1");

        return $procesos;

    }

    public function registro($transaccion, $comprobante){

        $registro = DB::table('v_cuenta AS c')->select('r.descripcion AS Referencia', 'ri.descripcion AS Referencia_i', 'rii.descripcion AS Referencia_ii')
            ->join('Transaccion AS p', 'c.Cuenta', 'p.Cuenta')
            ->join('g_registro AS t', 'p.Transaccion', 't.Registro')
            ->join('g_registro AS m', 'p.medio', 'm.Registro')
            ->join('g_registro AS r', 'p.Referencia', 'r.Registro')
            ->join('g_registro AS ri', 'p.Referencia_i', 'ri.Registro')
            ->join('g_registro AS rii', 'p.Referencia_ii', 'rii.Registro')
            ->where([
                ['c.Auxiliar', '=', session()->get('empresa')],
                ['p.Numero', '=', $transaccion],
                ['p.Transaccion', '=', $comprobante],
                ['t.tabla', '=', '182'],
                ['m.tabla', '=', '183'],
                ['r.tabla', '=', '184'],
                ['ri.tabla', '=', '184'],
                ['rii.tabla', '=', '184'],

            ])
            ->orderBy('Referencia');

        return $registro->get();
    }

    public function fecha($fechaIngresada){
        $empresa = session()->get('empresa');

        $fecha = DB::select("SELECT MAX(fecha)AS fecha FROM registro WHERE 
          comprobante=( SELECT base FROM base WHERE clasificacion=11607 LIMIT 1) AND empresa = $empresa");

        if($fecha[0]->fecha == null) {
            $resultado = 'true';
        }elseif($fecha[0]->fecha < $fechaIngresada OR $fecha[0]->fecha == $fechaIngresada){
            $resultado = 'true';
        }else{
            $resultado = 'false';
        }

        return $resultado;
    }


    public function lista(Request $request){

        $novedades = e_novedad::paginate(20);
        return view('novedad.lista', compact('novedades'));


    }

    public function listaOld(Request $request){

        if($request->cliente == "" AND $request->cedula == ""){
            $eneraciones = DB::table('v_mo_operacion')->orderBy('Operacion')->paginate(6);
            return view('operacion.lista', compact('operaciones'));
        }

        if($request->cliente != null AND $request->cedula == null){
            $eneraciones = DB::table('v_mo_operacion')->where('nombre', $request->cliente)->orderBy('Operacion')->paginate(6);
            return view('operacion.lista', compact('operaciones'));
        }

        if($request->cedula != null AND $request->cliente == null){
            $eneraciones = DB::table('v_mo_operacion')->where('auxiliar', $request->cedula)->orderBy('Operacion')->paginate(6);
            return view('operacion.lista', compact('operaciones'));
        }

    }

    public function download($name, $id){

        $path_web = '/home/ocobosoft/soporte/moperacion/'.$id.'/';
        $url = $name; // /storage/Modelo-Movil.xlsx
        $file= $path_web.$url; //home/ocobosoft/operacion/storage/app/public/Modelo-Movil.xlsx
        if(file_exists($file)){
            $headers = array(
                'Content-Type: ' . mime_content_type( $file ),
            );
            return Response::download($file, $name, $headers);
        }
        return 'No existe el archivo';

    }

    public function tercero($id)
    {

        $comp = (int)$id;
        $tercero = DB::select("SELECT DISTINCT v.auxiliar, v.nombre FROM base b, grupo_base g, grupo_auxiliar a, v_auxiliar v
                    WHERE b.Base = ? AND b.Base = g.Base AND g.Grupo = a.Grupo AND a.Auxiliar = v.auxiliar", [$comp]);

        return $tercero;

    }



}
