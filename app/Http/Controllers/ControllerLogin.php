<?php

namespace App\Http\Controllers;
use App\User;

class ControllerLogin extends Controller
{
    //
    public function __construct()
    {

        $this->middleware('guest')->except('logout');

    }

    public function index(){
        return view('auth.login');
    }

    public function loginMovil($auxiliar, $usuario, $password){

        $user = User::where(
            [
                ['usuario', $usuario],
                ['password', $password],
                ['auxiliar', $auxiliar]
            ]
        )->first();

        if($user == null){
            return json_encode('false');
        }

         return json_encode('true');

    }
}
