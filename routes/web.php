<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Jesus Antonio
Route::resource('novedades', 'NovedadController')->except([
    'show'
]);

//Ing Torres
Route::get('search/autocomplete', 'NovedadController@autocomplete');
Route::get('search/anotaciones', 'NovedadController@getAnotaciones');
Route::get('novedades/{predecesor}/tracing', 'NovedadController@tracing');

Route::get('/Proceso/{id}', ['as'=>'Proceso','uses'=>'ControllerNovedad@Proceso']);
Route::get('/Transaccion/{id}', ['as'=>'Transaccion','uses'=>'ControllerNovedad@Transaccion']);
Route::get('/Registro/{transaccion}/{comprobante}', ['as'=>'Registro','uses'=>'ControllerNovedad@Registro']);
Route::get('/Fecha/{fechaIngresada}', ['as'=>'Fecha','uses'=>'ControllerNovedad@Fecha']);

Route::get('/download/{name}/{id}', ['as'=>'download','uses'=>'ControllerNovedad@download']);

Route::get('/', 'ControllerLogin@index');

Route::get('/login', ['as'=>'login','uses'=>'Auth\LoginController@login']);
Route::get('/loginMovil/{auxiliar}/{usuario}/{pass}', ['as'=>'loginMovil','uses'=>'ControllerLogin@loginMovil']);
Route::get('/logout', ['as'=>'logout','uses'=>'Auth\LoginController@logout']);

Route::get('/tercero/{id}', ['as'=>'tercero','uses'=>'ControllerNovedad@tercero']);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/auxiliar', 'ControllerAuxiliar@sincronizarAuxiliar');
Route::get('/actualizar/{auxiliar}', 'ControllerAuxiliar@actualizarAuxiliar');


Route::post('/sincronizar', 'ControllerAuxiliar@sincronizarOperacionMovil');

//Route para traer el ID de la Operacion en la NUBE.
Route::get('/nube/{id}/{id_android}', ['as'=>'tercero','uses'=>'ControllerAuxiliar@nube']);
