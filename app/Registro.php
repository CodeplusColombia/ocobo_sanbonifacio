<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registro extends Model
{
    protected $table = "g_registro";
    protected $primaryKey = "Registro";
    public $timestamps = false;

    protected $fillable = ["Tabla",
        "Registro",
        "Descripcion",
        'Valor_i',
        'Valor_ii',
        'Valor_iii'];

    public function Novedad()
    {
        return $this->hasMany('App\Novedad', 'Clasificacion');
    }

    public function catedras()
    {
        return $this->hasMany('App\Catedra', 'Estado', 'Registro');
    }

    public function auxiliares()
    {
        return $this->hasMany('App\Auxiliar', 'Identificacion', 'Registro');
    }
}
