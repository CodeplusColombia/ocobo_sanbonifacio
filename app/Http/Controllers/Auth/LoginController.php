<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {

        $this->validate($request, [
            'usuario' => 'required',
        ]);

        $user = User::where(
            [
                ['usuario', $request->get('usuario')],
                ['password', $request->get('password')],
                ['auxiliar', $request->get('auxiliar')]
            ]
        )->first();


        if($user == null) {
            return back()->with('msg', 'Datos incorrectos, vuelve a intentarlo.');
        }



        Auth::login($user);

//        $permisos = DB::table('g_permiso AS p')->select('p.insertar','p.actualizar','p.borrar','p.seleccionar','p.ejecutar','r.valor_i AS alcance','a.empresa')
//            ->join('g_perfil AS u', 'p.usuario', 'u.usuario' AND 'p.aplicacion', 'u.aplicacion')
//            ->join('g_registro AS r', 'u.perfil', 'r.registro')
//            ->join('g_usuario AS x', 'u.usuario', 'x.usuario')
//            ->join('adicional AS a', 'x.auxiliar', 'a.auxiliar')
//            ->where([
//                ['u.usuario', '=', $request->usuario],
//                ['u.aplicacion', '=', '10201'],
//                ['p.opcion', '=', '10393'],
//            ]);


//        $permisos = DB::table('g_permiso AS p, g_perfil AS u, g_registro AS r, g_usuario AS x, adicional AS a')
//            ->select('p.insertar','p.actualizar','p.borrar','p.seleccionar','p.ejecutar','r.valor_i AS alcance','a.empresa')
//            ->where([
//                ['p.usuario', '=', 'u.usuario'],
//                ['p.aplicacion', '=', 'u.aplicacion'],
//                ['u.perfil', '=', 'r.registro'],
//                ['u.usuario', '=', 'x.usuario'],
//                ['x.auxiliar', '=', 'a.auxiliar'],
//                ['u.usuario', '=', $request->usuario],
//                ['u.aplicacion', '=', '10201'],
//                ['p.opcion', '=', '10393'],
//            ]);

        $permisos = DB::select("SELECT p.insertar,p.actualizar,p.borrar,p.seleccionar,p.ejecutar,r.valor_i AS alcance,a.empresa 
                                FROM g_permiso p, g_perfil u, g_registro r, g_usuario x, adicional a 
                                WHERE p.usuario=u.usuario 
                                AND p.aplicacion=u.aplicacion 
                                AND u.perfil=r.registro 
                                AND u.usuario=x.usuario 
                                AND x.auxiliar=a.auxiliar 
                                AND u.usuario= ? 
                                And u.aplicacion=10201 
                                And p.opcion=10393", [$request->usuario]);


        session()->put('usuario', $request->get('usuario'));
        session()->put('empresa', $request->get('auxiliar'));
        session()->put('insertar', $permisos[0]->insertar);
        session()->put('actualizar', $permisos[0]->actualizar);
        session()->put('borrar', $permisos[0]->borrar);
        session()->put('seleccionar', $permisos[0]->seleccionar);
        session()->put('ejecutar', $permisos[0]->ejecutar);
        session()->put('alcance', $permisos[0]->alcance);
        session()->put('empresa', $permisos[0]->empresa);


        if(strtoupper(session()->get('alcance')) == "GENERAL"){
            session()->put('alcance_usuario', "");
        }

        if(strtoupper(session()->get('alcance')) == "SEDE"){
            session()->put('alcance_usuario', 'sede='.session()->get('empresa'));

        }

        if(strtoupper(session()->get('alcance')) == "USUARIO"){
            session()->put('alcance_usuario', 'usuario='.session()->get('usuario'));

        }

        if(strtoupper(session()->get('alcance')) == "COORDINADOR"){
            session()->put('alcance_usuario', 'sede='.session()->get('empresa'));
            $this->darCoordinador(session()->get('usuario'));
        }

        //$this->darCoordinador('Ocobo');
        return Redirect::to('/');
    }

    public function darCoordinador($usuario){

        $resultado = 0;
        $consultar = DB::select("SELECT grupo FROM e_grupo WHERE estado=801005 AND coordinador=(SELECT auxiliar FROM g_usuario WHERE usuario= ? )", [$usuario]);

        if(sizeof($consultar) == 0){
            $resultado = $consultar;
        }

        $complemento = 'AND v.novedad IN(SELECT n.novedad FROM e_novedad n, e_registro r, e_catedra c, e_grupo g WHERE n.registro=r.registro AND r.catedra=c.catedra AND c.grupo=g.grupo AND g.grupo IN($resultado) ); AND v.registro IN(SELECT r.registro FROM e_registro r, e_catedra c, e_grupo g WHERE r.catedra=c.catedra AND c.grupo=g.grupo AND g.grupo IN($resultado) )';
        session()->put('coordinador', $complemento);
        dd(session()->get('coordinador'));
    }


    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/');
    }

}
