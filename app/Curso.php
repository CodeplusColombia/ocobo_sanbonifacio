<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $table = "e_curso";
    protected $primaryKey = "Curso";
    public $timestamps = false;

    protected $fillable = ["Curso",
        "Grupo",
        "Descripcion",
        'Observacion',
        'Nomenclatura',
        'Cupos',
        'Jornada',
        'Estado'];

    public function grupos()
    {
        return $this->hasMany('App\Grupo', 'Grupo', 'Grupo');
    }
}
